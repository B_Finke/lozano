################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/gpu/cudpp_mini/cudpp.cpp \
../lib/gpu/cudpp_mini/cudpp_maximal_launch.cpp \
../lib/gpu/cudpp_mini/cudpp_plan.cpp \
../lib/gpu/cudpp_mini/cudpp_plan_manager.cpp 

OBJS += \
./lib/gpu/cudpp_mini/cudpp.o \
./lib/gpu/cudpp_mini/cudpp_maximal_launch.o \
./lib/gpu/cudpp_mini/cudpp_plan.o \
./lib/gpu/cudpp_mini/cudpp_plan_manager.o 

CPP_DEPS += \
./lib/gpu/cudpp_mini/cudpp.d \
./lib/gpu/cudpp_mini/cudpp_maximal_launch.d \
./lib/gpu/cudpp_mini/cudpp_plan.d \
./lib/gpu/cudpp_mini/cudpp_plan_manager.d 


# Each subdirectory must supply rules for building sources it contributes
lib/gpu/cudpp_mini/%.o: ../lib/gpu/cudpp_mini/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


