################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/poems/body.cpp \
../lib/poems/body23joint.cpp \
../lib/poems/colmatmap.cpp \
../lib/poems/colmatrix.cpp \
../lib/poems/eulerparameters.cpp \
../lib/poems/fastmatrixops.cpp \
../lib/poems/fixedpoint.cpp \
../lib/poems/freebodyjoint.cpp \
../lib/poems/inertialframe.cpp \
../lib/poems/joint.cpp \
../lib/poems/mat3x3.cpp \
../lib/poems/mat4x4.cpp \
../lib/poems/mat6x6.cpp \
../lib/poems/matrix.cpp \
../lib/poems/matrixfun.cpp \
../lib/poems/mixedjoint.cpp \
../lib/poems/norm.cpp \
../lib/poems/onbody.cpp \
../lib/poems/onfunctions.cpp \
../lib/poems/onsolver.cpp \
../lib/poems/particle.cpp \
../lib/poems/poemsobject.cpp \
../lib/poems/poemstreenode.cpp \
../lib/poems/point.cpp \
../lib/poems/prismaticjoint.cpp \
../lib/poems/revolutejoint.cpp \
../lib/poems/rigidbody.cpp \
../lib/poems/rowmatrix.cpp \
../lib/poems/solver.cpp \
../lib/poems/sphericalFlexiblejoint.cpp \
../lib/poems/sphericaljoint.cpp \
../lib/poems/system.cpp \
../lib/poems/vect3.cpp \
../lib/poems/vect4.cpp \
../lib/poems/vect6.cpp \
../lib/poems/virtualcolmatrix.cpp \
../lib/poems/virtualmatrix.cpp \
../lib/poems/virtualrowmatrix.cpp \
../lib/poems/workspace.cpp 

OBJS += \
./lib/poems/body.o \
./lib/poems/body23joint.o \
./lib/poems/colmatmap.o \
./lib/poems/colmatrix.o \
./lib/poems/eulerparameters.o \
./lib/poems/fastmatrixops.o \
./lib/poems/fixedpoint.o \
./lib/poems/freebodyjoint.o \
./lib/poems/inertialframe.o \
./lib/poems/joint.o \
./lib/poems/mat3x3.o \
./lib/poems/mat4x4.o \
./lib/poems/mat6x6.o \
./lib/poems/matrix.o \
./lib/poems/matrixfun.o \
./lib/poems/mixedjoint.o \
./lib/poems/norm.o \
./lib/poems/onbody.o \
./lib/poems/onfunctions.o \
./lib/poems/onsolver.o \
./lib/poems/particle.o \
./lib/poems/poemsobject.o \
./lib/poems/poemstreenode.o \
./lib/poems/point.o \
./lib/poems/prismaticjoint.o \
./lib/poems/revolutejoint.o \
./lib/poems/rigidbody.o \
./lib/poems/rowmatrix.o \
./lib/poems/solver.o \
./lib/poems/sphericalFlexiblejoint.o \
./lib/poems/sphericaljoint.o \
./lib/poems/system.o \
./lib/poems/vect3.o \
./lib/poems/vect4.o \
./lib/poems/vect6.o \
./lib/poems/virtualcolmatrix.o \
./lib/poems/virtualmatrix.o \
./lib/poems/virtualrowmatrix.o \
./lib/poems/workspace.o 

CPP_DEPS += \
./lib/poems/body.d \
./lib/poems/body23joint.d \
./lib/poems/colmatmap.d \
./lib/poems/colmatrix.d \
./lib/poems/eulerparameters.d \
./lib/poems/fastmatrixops.d \
./lib/poems/fixedpoint.d \
./lib/poems/freebodyjoint.d \
./lib/poems/inertialframe.d \
./lib/poems/joint.d \
./lib/poems/mat3x3.d \
./lib/poems/mat4x4.d \
./lib/poems/mat6x6.d \
./lib/poems/matrix.d \
./lib/poems/matrixfun.d \
./lib/poems/mixedjoint.d \
./lib/poems/norm.d \
./lib/poems/onbody.d \
./lib/poems/onfunctions.d \
./lib/poems/onsolver.d \
./lib/poems/particle.d \
./lib/poems/poemsobject.d \
./lib/poems/poemstreenode.d \
./lib/poems/point.d \
./lib/poems/prismaticjoint.d \
./lib/poems/revolutejoint.d \
./lib/poems/rigidbody.d \
./lib/poems/rowmatrix.d \
./lib/poems/solver.d \
./lib/poems/sphericalFlexiblejoint.d \
./lib/poems/sphericaljoint.d \
./lib/poems/system.d \
./lib/poems/vect3.d \
./lib/poems/vect4.d \
./lib/poems/vect6.d \
./lib/poems/virtualcolmatrix.d \
./lib/poems/virtualmatrix.d \
./lib/poems/virtualrowmatrix.d \
./lib/poems/workspace.d 


# Each subdirectory must supply rules for building sources it contributes
lib/poems/%.o: ../lib/poems/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


