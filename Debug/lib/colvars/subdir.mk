################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/colvars/colvar.cpp \
../lib/colvars/colvaratoms.cpp \
../lib/colvars/colvarbias.cpp \
../lib/colvars/colvarbias_abf.cpp \
../lib/colvars/colvarbias_meta.cpp \
../lib/colvars/colvarcomp.cpp \
../lib/colvars/colvarcomp_angles.cpp \
../lib/colvars/colvarcomp_coordnums.cpp \
../lib/colvars/colvarcomp_distances.cpp \
../lib/colvars/colvarcomp_protein.cpp \
../lib/colvars/colvarcomp_rotations.cpp \
../lib/colvars/colvargrid.cpp \
../lib/colvars/colvarmodule.cpp \
../lib/colvars/colvarparse.cpp \
../lib/colvars/colvarvalue.cpp 

OBJS += \
./lib/colvars/colvar.o \
./lib/colvars/colvaratoms.o \
./lib/colvars/colvarbias.o \
./lib/colvars/colvarbias_abf.o \
./lib/colvars/colvarbias_meta.o \
./lib/colvars/colvarcomp.o \
./lib/colvars/colvarcomp_angles.o \
./lib/colvars/colvarcomp_coordnums.o \
./lib/colvars/colvarcomp_distances.o \
./lib/colvars/colvarcomp_protein.o \
./lib/colvars/colvarcomp_rotations.o \
./lib/colvars/colvargrid.o \
./lib/colvars/colvarmodule.o \
./lib/colvars/colvarparse.o \
./lib/colvars/colvarvalue.o 

CPP_DEPS += \
./lib/colvars/colvar.d \
./lib/colvars/colvaratoms.d \
./lib/colvars/colvarbias.d \
./lib/colvars/colvarbias_abf.d \
./lib/colvars/colvarbias_meta.d \
./lib/colvars/colvarcomp.d \
./lib/colvars/colvarcomp_angles.d \
./lib/colvars/colvarcomp_coordnums.d \
./lib/colvars/colvarcomp_distances.d \
./lib/colvars/colvarcomp_protein.d \
./lib/colvars/colvarcomp_rotations.d \
./lib/colvars/colvargrid.d \
./lib/colvars/colvarmodule.d \
./lib/colvars/colvarparse.d \
./lib/colvars/colvarvalue.d 


# Each subdirectory must supply rules for building sources it contributes
lib/colvars/%.o: ../lib/colvars/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


