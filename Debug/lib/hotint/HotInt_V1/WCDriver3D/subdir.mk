################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/WCDriver3D/CListDlg.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/ComputeEigenmodes.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/CustomEditDialog.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DataStorage.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogDataManager.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogFramesRecording.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogProgressBar.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogReadText.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogSaveSpecial.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DialogViewingOptions.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DlgBodyJointOpt.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DlgOneEditCtrl.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/DrawWindow2D.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/FEDrawingOptions.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/GLDrawWnd.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/MyBaseView.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/OpenGLDlg.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/OutputDialog.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg_aux.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/RenderContext.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/StdAfx.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/TreeViewCustomEditDialog.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/WCDriver3D.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/WCDriver3DDlg.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/extgl.cpp \
../lib/hotint/HotInt_V1/WCDriver3D/savewindowbitmap.cpp 

OBJS += \
./lib/hotint/HotInt_V1/WCDriver3D/CListDlg.o \
./lib/hotint/HotInt_V1/WCDriver3D/ComputeEigenmodes.o \
./lib/hotint/HotInt_V1/WCDriver3D/CustomEditDialog.o \
./lib/hotint/HotInt_V1/WCDriver3D/DataStorage.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogDataManager.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogFramesRecording.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogProgressBar.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogReadText.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogSaveSpecial.o \
./lib/hotint/HotInt_V1/WCDriver3D/DialogViewingOptions.o \
./lib/hotint/HotInt_V1/WCDriver3D/DlgBodyJointOpt.o \
./lib/hotint/HotInt_V1/WCDriver3D/DlgOneEditCtrl.o \
./lib/hotint/HotInt_V1/WCDriver3D/DrawWindow2D.o \
./lib/hotint/HotInt_V1/WCDriver3D/FEDrawingOptions.o \
./lib/hotint/HotInt_V1/WCDriver3D/GLDrawWnd.o \
./lib/hotint/HotInt_V1/WCDriver3D/MyBaseView.o \
./lib/hotint/HotInt_V1/WCDriver3D/OpenGLDlg.o \
./lib/hotint/HotInt_V1/WCDriver3D/OutputDialog.o \
./lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg.o \
./lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg_aux.o \
./lib/hotint/HotInt_V1/WCDriver3D/RenderContext.o \
./lib/hotint/HotInt_V1/WCDriver3D/StdAfx.o \
./lib/hotint/HotInt_V1/WCDriver3D/TreeViewCustomEditDialog.o \
./lib/hotint/HotInt_V1/WCDriver3D/WCDriver3D.o \
./lib/hotint/HotInt_V1/WCDriver3D/WCDriver3DDlg.o \
./lib/hotint/HotInt_V1/WCDriver3D/extgl.o \
./lib/hotint/HotInt_V1/WCDriver3D/savewindowbitmap.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/WCDriver3D/CListDlg.d \
./lib/hotint/HotInt_V1/WCDriver3D/ComputeEigenmodes.d \
./lib/hotint/HotInt_V1/WCDriver3D/CustomEditDialog.d \
./lib/hotint/HotInt_V1/WCDriver3D/DataStorage.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogDataManager.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogFramesRecording.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogProgressBar.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogReadText.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogSaveSpecial.d \
./lib/hotint/HotInt_V1/WCDriver3D/DialogViewingOptions.d \
./lib/hotint/HotInt_V1/WCDriver3D/DlgBodyJointOpt.d \
./lib/hotint/HotInt_V1/WCDriver3D/DlgOneEditCtrl.d \
./lib/hotint/HotInt_V1/WCDriver3D/DrawWindow2D.d \
./lib/hotint/HotInt_V1/WCDriver3D/FEDrawingOptions.d \
./lib/hotint/HotInt_V1/WCDriver3D/GLDrawWnd.d \
./lib/hotint/HotInt_V1/WCDriver3D/MyBaseView.d \
./lib/hotint/HotInt_V1/WCDriver3D/OpenGLDlg.d \
./lib/hotint/HotInt_V1/WCDriver3D/OutputDialog.d \
./lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg.d \
./lib/hotint/HotInt_V1/WCDriver3D/PlotToolDlg_aux.d \
./lib/hotint/HotInt_V1/WCDriver3D/RenderContext.d \
./lib/hotint/HotInt_V1/WCDriver3D/StdAfx.d \
./lib/hotint/HotInt_V1/WCDriver3D/TreeViewCustomEditDialog.d \
./lib/hotint/HotInt_V1/WCDriver3D/WCDriver3D.d \
./lib/hotint/HotInt_V1/WCDriver3D/WCDriver3DDlg.d \
./lib/hotint/HotInt_V1/WCDriver3D/extgl.d \
./lib/hotint/HotInt_V1/WCDriver3D/savewindowbitmap.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/WCDriver3D/%.o: ../lib/hotint/HotInt_V1/WCDriver3D/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


