################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ServiceObjectsLib/GeomElements.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/MBSload.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/Sim2Hotint.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/TcpIpRoutines.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/csg_geometry.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/material.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/model_tools.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/node.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/sensorProcessorsSpecific.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/sensors.cpp \
../lib/hotint/HotInt_V1/ServiceObjectsLib/sensorsSpecific.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ServiceObjectsLib/GeomElements.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/MBSload.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/Sim2Hotint.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/TcpIpRoutines.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/csg_geometry.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/material.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/model_tools.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/node.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensorProcessorsSpecific.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensors.o \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensorsSpecific.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ServiceObjectsLib/GeomElements.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/MBSload.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/Sim2Hotint.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/TcpIpRoutines.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/csg_geometry.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/material.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/model_tools.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/node.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensorProcessorsSpecific.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensors.d \
./lib/hotint/HotInt_V1/ServiceObjectsLib/sensorsSpecific.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ServiceObjectsLib/%.o: ../lib/hotint/HotInt_V1/ServiceObjectsLib/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


