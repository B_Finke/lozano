################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/SuperLU/SuperLUmain.cpp 

C_SRCS += \
../lib/hotint/HotInt_V1/SuperLU/colamd.c \
../lib/hotint/HotInt_V1/SuperLU/dasum.c \
../lib/hotint/HotInt_V1/SuperLU/daxpy.c \
../lib/hotint/HotInt_V1/SuperLU/dcolumn_bmod.c \
../lib/hotint/HotInt_V1/SuperLU/dcolumn_dfs.c \
../lib/hotint/HotInt_V1/SuperLU/dcopy.c \
../lib/hotint/HotInt_V1/SuperLU/dcopy_to_ucol.c \
../lib/hotint/HotInt_V1/SuperLU/ddot.c \
../lib/hotint/HotInt_V1/SuperLU/dgemv.c \
../lib/hotint/HotInt_V1/SuperLU/dger.c \
../lib/hotint/HotInt_V1/SuperLU/dgscon.c \
../lib/hotint/HotInt_V1/SuperLU/dgsequ.c \
../lib/hotint/HotInt_V1/SuperLU/dgsrfs.c \
../lib/hotint/HotInt_V1/SuperLU/dgssv.c \
../lib/hotint/HotInt_V1/SuperLU/dgssvx.c \
../lib/hotint/HotInt_V1/SuperLU/dgstrf.c \
../lib/hotint/HotInt_V1/SuperLU/dgstrs.c \
../lib/hotint/HotInt_V1/SuperLU/dlacon.c \
../lib/hotint/HotInt_V1/SuperLU/dlamch.c \
../lib/hotint/HotInt_V1/SuperLU/dlangs.c \
../lib/hotint/HotInt_V1/SuperLU/dlaqgs.c \
../lib/hotint/HotInt_V1/SuperLU/dmemory.c \
../lib/hotint/HotInt_V1/SuperLU/dmyblas2.c \
../lib/hotint/HotInt_V1/SuperLU/dnrm2.c \
../lib/hotint/HotInt_V1/SuperLU/dpanel_bmod.c \
../lib/hotint/HotInt_V1/SuperLU/dpanel_dfs.c \
../lib/hotint/HotInt_V1/SuperLU/dpivotL.c \
../lib/hotint/HotInt_V1/SuperLU/dpivotgrowth.c \
../lib/hotint/HotInt_V1/SuperLU/dpruneL.c \
../lib/hotint/HotInt_V1/SuperLU/dreadhb.c \
../lib/hotint/HotInt_V1/SuperLU/drot.c \
../lib/hotint/HotInt_V1/SuperLU/dscal.c \
../lib/hotint/HotInt_V1/SuperLU/dsnode_bmod.c \
../lib/hotint/HotInt_V1/SuperLU/dsnode_dfs.c \
../lib/hotint/HotInt_V1/SuperLU/dsp_blas2.c \
../lib/hotint/HotInt_V1/SuperLU/dsp_blas3.c \
../lib/hotint/HotInt_V1/SuperLU/dsymv.c \
../lib/hotint/HotInt_V1/SuperLU/dsyr2.c \
../lib/hotint/HotInt_V1/SuperLU/dtrsv.c \
../lib/hotint/HotInt_V1/SuperLU/dutil.c \
../lib/hotint/HotInt_V1/SuperLU/get_perm_c.c \
../lib/hotint/HotInt_V1/SuperLU/heap_relax_snode.c \
../lib/hotint/HotInt_V1/SuperLU/idamax.c \
../lib/hotint/HotInt_V1/SuperLU/lsame.c \
../lib/hotint/HotInt_V1/SuperLU/memory.c \
../lib/hotint/HotInt_V1/SuperLU/mmd.c \
../lib/hotint/HotInt_V1/SuperLU/relax_snode.c \
../lib/hotint/HotInt_V1/SuperLU/sp_coletree.c \
../lib/hotint/HotInt_V1/SuperLU/sp_ienv.c \
../lib/hotint/HotInt_V1/SuperLU/sp_preorder.c \
../lib/hotint/HotInt_V1/SuperLU/superlu.c \
../lib/hotint/HotInt_V1/SuperLU/superlu_timer.c \
../lib/hotint/HotInt_V1/SuperLU/util.c \
../lib/hotint/HotInt_V1/SuperLU/xerbla.c 

OBJS += \
./lib/hotint/HotInt_V1/SuperLU/SuperLUmain.o \
./lib/hotint/HotInt_V1/SuperLU/colamd.o \
./lib/hotint/HotInt_V1/SuperLU/dasum.o \
./lib/hotint/HotInt_V1/SuperLU/daxpy.o \
./lib/hotint/HotInt_V1/SuperLU/dcolumn_bmod.o \
./lib/hotint/HotInt_V1/SuperLU/dcolumn_dfs.o \
./lib/hotint/HotInt_V1/SuperLU/dcopy.o \
./lib/hotint/HotInt_V1/SuperLU/dcopy_to_ucol.o \
./lib/hotint/HotInt_V1/SuperLU/ddot.o \
./lib/hotint/HotInt_V1/SuperLU/dgemv.o \
./lib/hotint/HotInt_V1/SuperLU/dger.o \
./lib/hotint/HotInt_V1/SuperLU/dgscon.o \
./lib/hotint/HotInt_V1/SuperLU/dgsequ.o \
./lib/hotint/HotInt_V1/SuperLU/dgsrfs.o \
./lib/hotint/HotInt_V1/SuperLU/dgssv.o \
./lib/hotint/HotInt_V1/SuperLU/dgssvx.o \
./lib/hotint/HotInt_V1/SuperLU/dgstrf.o \
./lib/hotint/HotInt_V1/SuperLU/dgstrs.o \
./lib/hotint/HotInt_V1/SuperLU/dlacon.o \
./lib/hotint/HotInt_V1/SuperLU/dlamch.o \
./lib/hotint/HotInt_V1/SuperLU/dlangs.o \
./lib/hotint/HotInt_V1/SuperLU/dlaqgs.o \
./lib/hotint/HotInt_V1/SuperLU/dmemory.o \
./lib/hotint/HotInt_V1/SuperLU/dmyblas2.o \
./lib/hotint/HotInt_V1/SuperLU/dnrm2.o \
./lib/hotint/HotInt_V1/SuperLU/dpanel_bmod.o \
./lib/hotint/HotInt_V1/SuperLU/dpanel_dfs.o \
./lib/hotint/HotInt_V1/SuperLU/dpivotL.o \
./lib/hotint/HotInt_V1/SuperLU/dpivotgrowth.o \
./lib/hotint/HotInt_V1/SuperLU/dpruneL.o \
./lib/hotint/HotInt_V1/SuperLU/dreadhb.o \
./lib/hotint/HotInt_V1/SuperLU/drot.o \
./lib/hotint/HotInt_V1/SuperLU/dscal.o \
./lib/hotint/HotInt_V1/SuperLU/dsnode_bmod.o \
./lib/hotint/HotInt_V1/SuperLU/dsnode_dfs.o \
./lib/hotint/HotInt_V1/SuperLU/dsp_blas2.o \
./lib/hotint/HotInt_V1/SuperLU/dsp_blas3.o \
./lib/hotint/HotInt_V1/SuperLU/dsymv.o \
./lib/hotint/HotInt_V1/SuperLU/dsyr2.o \
./lib/hotint/HotInt_V1/SuperLU/dtrsv.o \
./lib/hotint/HotInt_V1/SuperLU/dutil.o \
./lib/hotint/HotInt_V1/SuperLU/get_perm_c.o \
./lib/hotint/HotInt_V1/SuperLU/heap_relax_snode.o \
./lib/hotint/HotInt_V1/SuperLU/idamax.o \
./lib/hotint/HotInt_V1/SuperLU/lsame.o \
./lib/hotint/HotInt_V1/SuperLU/memory.o \
./lib/hotint/HotInt_V1/SuperLU/mmd.o \
./lib/hotint/HotInt_V1/SuperLU/relax_snode.o \
./lib/hotint/HotInt_V1/SuperLU/sp_coletree.o \
./lib/hotint/HotInt_V1/SuperLU/sp_ienv.o \
./lib/hotint/HotInt_V1/SuperLU/sp_preorder.o \
./lib/hotint/HotInt_V1/SuperLU/superlu.o \
./lib/hotint/HotInt_V1/SuperLU/superlu_timer.o \
./lib/hotint/HotInt_V1/SuperLU/util.o \
./lib/hotint/HotInt_V1/SuperLU/xerbla.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/SuperLU/SuperLUmain.d 

C_DEPS += \
./lib/hotint/HotInt_V1/SuperLU/colamd.d \
./lib/hotint/HotInt_V1/SuperLU/dasum.d \
./lib/hotint/HotInt_V1/SuperLU/daxpy.d \
./lib/hotint/HotInt_V1/SuperLU/dcolumn_bmod.d \
./lib/hotint/HotInt_V1/SuperLU/dcolumn_dfs.d \
./lib/hotint/HotInt_V1/SuperLU/dcopy.d \
./lib/hotint/HotInt_V1/SuperLU/dcopy_to_ucol.d \
./lib/hotint/HotInt_V1/SuperLU/ddot.d \
./lib/hotint/HotInt_V1/SuperLU/dgemv.d \
./lib/hotint/HotInt_V1/SuperLU/dger.d \
./lib/hotint/HotInt_V1/SuperLU/dgscon.d \
./lib/hotint/HotInt_V1/SuperLU/dgsequ.d \
./lib/hotint/HotInt_V1/SuperLU/dgsrfs.d \
./lib/hotint/HotInt_V1/SuperLU/dgssv.d \
./lib/hotint/HotInt_V1/SuperLU/dgssvx.d \
./lib/hotint/HotInt_V1/SuperLU/dgstrf.d \
./lib/hotint/HotInt_V1/SuperLU/dgstrs.d \
./lib/hotint/HotInt_V1/SuperLU/dlacon.d \
./lib/hotint/HotInt_V1/SuperLU/dlamch.d \
./lib/hotint/HotInt_V1/SuperLU/dlangs.d \
./lib/hotint/HotInt_V1/SuperLU/dlaqgs.d \
./lib/hotint/HotInt_V1/SuperLU/dmemory.d \
./lib/hotint/HotInt_V1/SuperLU/dmyblas2.d \
./lib/hotint/HotInt_V1/SuperLU/dnrm2.d \
./lib/hotint/HotInt_V1/SuperLU/dpanel_bmod.d \
./lib/hotint/HotInt_V1/SuperLU/dpanel_dfs.d \
./lib/hotint/HotInt_V1/SuperLU/dpivotL.d \
./lib/hotint/HotInt_V1/SuperLU/dpivotgrowth.d \
./lib/hotint/HotInt_V1/SuperLU/dpruneL.d \
./lib/hotint/HotInt_V1/SuperLU/dreadhb.d \
./lib/hotint/HotInt_V1/SuperLU/drot.d \
./lib/hotint/HotInt_V1/SuperLU/dscal.d \
./lib/hotint/HotInt_V1/SuperLU/dsnode_bmod.d \
./lib/hotint/HotInt_V1/SuperLU/dsnode_dfs.d \
./lib/hotint/HotInt_V1/SuperLU/dsp_blas2.d \
./lib/hotint/HotInt_V1/SuperLU/dsp_blas3.d \
./lib/hotint/HotInt_V1/SuperLU/dsymv.d \
./lib/hotint/HotInt_V1/SuperLU/dsyr2.d \
./lib/hotint/HotInt_V1/SuperLU/dtrsv.d \
./lib/hotint/HotInt_V1/SuperLU/dutil.d \
./lib/hotint/HotInt_V1/SuperLU/get_perm_c.d \
./lib/hotint/HotInt_V1/SuperLU/heap_relax_snode.d \
./lib/hotint/HotInt_V1/SuperLU/idamax.d \
./lib/hotint/HotInt_V1/SuperLU/lsame.d \
./lib/hotint/HotInt_V1/SuperLU/memory.d \
./lib/hotint/HotInt_V1/SuperLU/mmd.d \
./lib/hotint/HotInt_V1/SuperLU/relax_snode.d \
./lib/hotint/HotInt_V1/SuperLU/sp_coletree.d \
./lib/hotint/HotInt_V1/SuperLU/sp_ienv.d \
./lib/hotint/HotInt_V1/SuperLU/sp_preorder.d \
./lib/hotint/HotInt_V1/SuperLU/superlu.d \
./lib/hotint/HotInt_V1/SuperLU/superlu_timer.d \
./lib/hotint/HotInt_V1/SuperLU/util.d \
./lib/hotint/HotInt_V1/SuperLU/xerbla.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/SuperLU/%.o: ../lib/hotint/HotInt_V1/SuperLU/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/hotint/HotInt_V1/SuperLU/%.o: ../lib/hotint/HotInt_V1/SuperLU/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


