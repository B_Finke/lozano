################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/BaseCMSElement.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement2D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSElement.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSRotorElement.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe2D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe3D.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/BaseCMSElement.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement2D.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSElement.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSRotorElement.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe2D.o \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe3D.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/BaseCMSElement.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/CMSElement2D.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSElement.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/GCMSRotorElement.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe2D.d \
./lib/hotint/HotInt_V1/ElementsLib/modal_reduction/referenceframe3D.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ElementsLib/modal_reduction/%.o: ../lib/hotint/HotInt_V1/ElementsLib/modal_reduction/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


