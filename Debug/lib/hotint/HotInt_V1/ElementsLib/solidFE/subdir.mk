################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FE2DTriQuad.cpp \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FE3DHexTet.cpp \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement2D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3DFFRF.cpp \
../lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElementGeneric.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FE2DTriQuad.o \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FE3DHexTet.o \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement2D.o \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3D.o \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3DFFRF.o \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElementGeneric.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FE2DTriQuad.d \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FE3DHexTet.d \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement2D.d \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3D.d \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElement3DFFRF.d \
./lib/hotint/HotInt_V1/ElementsLib/solidFE/FiniteElementGeneric.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ElementsLib/solidFE/%.o: ../lib/hotint/HotInt_V1/ElementsLib/solidFE/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


