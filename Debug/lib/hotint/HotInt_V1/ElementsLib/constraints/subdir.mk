################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ElementsLib/constraints/AverageConstraint.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/KinematicPairs.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/RigidBodyJoints.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/SDA_constraints.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/SpecialConstraints.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/constraint.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/contact2D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/contact3D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/constraints/control.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ElementsLib/constraints/AverageConstraint.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/KinematicPairs.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/RigidBodyJoints.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/SDA_constraints.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/SpecialConstraints.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/constraint.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/contact2D.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/contact3D.o \
./lib/hotint/HotInt_V1/ElementsLib/constraints/control.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ElementsLib/constraints/AverageConstraint.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/KinematicPairs.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/RigidBodyJoints.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/SDA_constraints.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/SpecialConstraints.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/constraint.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/contact2D.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/contact3D.d \
./lib/hotint/HotInt_V1/ElementsLib/constraints/control.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ElementsLib/constraints/%.o: ../lib/hotint/HotInt_V1/ElementsLib/constraints/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


