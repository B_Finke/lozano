################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ElementsLib/shells/ANCFPlate3D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/shells/ANCFSimpleThinPlate3D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/shells/ANCFThinPlate3D.cpp \
../lib/hotint/HotInt_V1/ElementsLib/shells/Plate2D.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFPlate3D.o \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFSimpleThinPlate3D.o \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFThinPlate3D.o \
./lib/hotint/HotInt_V1/ElementsLib/shells/Plate2D.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFPlate3D.d \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFSimpleThinPlate3D.d \
./lib/hotint/HotInt_V1/ElementsLib/shells/ANCFThinPlate3D.d \
./lib/hotint/HotInt_V1/ElementsLib/shells/Plate2D.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ElementsLib/shells/%.o: ../lib/hotint/HotInt_V1/ElementsLib/shells/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


