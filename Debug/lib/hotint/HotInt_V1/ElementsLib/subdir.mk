################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../lib/hotint/HotInt_V1/ElementsLib/ElementEDCauto.cpp \
../lib/hotint/HotInt_V1/ElementsLib/IntegrationRule.cpp \
../lib/hotint/HotInt_V1/ElementsLib/PlaneSymmetricTensorComponents.cpp \
../lib/hotint/HotInt_V1/ElementsLib/element.cpp 

OBJS += \
./lib/hotint/HotInt_V1/ElementsLib/ElementEDCauto.o \
./lib/hotint/HotInt_V1/ElementsLib/IntegrationRule.o \
./lib/hotint/HotInt_V1/ElementsLib/PlaneSymmetricTensorComponents.o \
./lib/hotint/HotInt_V1/ElementsLib/element.o 

CPP_DEPS += \
./lib/hotint/HotInt_V1/ElementsLib/ElementEDCauto.d \
./lib/hotint/HotInt_V1/ElementsLib/IntegrationRule.d \
./lib/hotint/HotInt_V1/ElementsLib/PlaneSymmetricTensorComponents.d \
./lib/hotint/HotInt_V1/ElementsLib/element.d 


# Each subdirectory must supply rules for building sources it contributes
lib/hotint/HotInt_V1/ElementsLib/%.o: ../lib/hotint/HotInt_V1/ElementsLib/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


