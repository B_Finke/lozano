################################################################################
# Automatically-generated file. Do not edit!
################################################################################

C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
CC_SRCS := 
ASM_SRCS := 
CPP_SRCS := 
C_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
CC_DEPS := 
C++_DEPS := 
EXECUTABLES := 
C_UPPER_DEPS := 
CXX_DEPS := 
OBJS := 
CPP_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
src \
src/WINDOWS/extra \
src/VORONOI \
src/STUBS \
src/RIGID \
src/POEMS \
src/PASCAL \
src/Obj_auto \
src/MOLECULE \
src/ASPHERE \
lib/poems \
lib/hotint/HotInt_V1/tools/HOTINT_Log/VC++\ solution/HOTINT_Log \
lib/hotint/HotInt_V1/tools/EDC_converter \
lib/hotint/HotInt_V1/WorkingModule \
lib/hotint/HotInt_V1/WCDriver3D \
lib/hotint/HotInt_V1/UtilityLib \
lib/hotint/HotInt_V1/SuperLU \
lib/hotint/HotInt_V1/ServiceObjectsLib \
lib/hotint/HotInt_V1/Parser \
lib/hotint/HotInt_V1/ModelsLib/open_source_examples/hourglass_rigid_contact_2D \
lib/hotint/HotInt_V1/ModelsLib/open_source_examples/flexible_web_contact_3D \
lib/hotint/HotInt_V1/ModelsLib/open_source_examples/ANCFCable2D_contact \
lib/hotint/HotInt_V1/MBSKernelLib \
lib/hotint/HotInt_V1/MBSElementsAndModels \
lib/hotint/HotInt_V1/FEMesh \
lib/hotint/HotInt_V1/ElementsLib/solidFE \
lib/hotint/HotInt_V1/ElementsLib/shells \
lib/hotint/HotInt_V1/ElementsLib/rigid \
lib/hotint/HotInt_V1/ElementsLib/modal_reduction \
lib/hotint/HotInt_V1/ElementsLib \
lib/hotint/HotInt_V1/ElementsLib/deprecated_elements \
lib/hotint/HotInt_V1/ElementsLib/constraints \
lib/hotint/HotInt_V1/ElementsLib/beams \
lib/gpu \
lib/gpu/geryon \
lib/gpu/cudpp_mini \
lib/colvars \
lib/awpmd/systems/interact/TCP \
lib/awpmd/ivutils/src \
lib/atc \

