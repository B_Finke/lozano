/* ----------------------------------------------------------------------
    This is the

    ██╗     ██╗ ██████╗  ██████╗  ██████╗ ██╗  ██╗████████╗███████╗
    ██║     ██║██╔════╝ ██╔════╝ ██╔════╝ ██║  ██║╚══██╔══╝██╔════╝
    ██║     ██║██║  ███╗██║  ███╗██║  ███╗███████║   ██║   ███████╗
    ██║     ██║██║   ██║██║   ██║██║   ██║██╔══██║   ██║   ╚════██║
    ███████╗██║╚██████╔╝╚██████╔╝╚██████╔╝██║  ██║   ██║   ███████║
    ╚══════╝╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝®

    DEM simulation engine, released by
    DCS Computing Gmbh, Linz, Austria
    http://www.dcs-computing.com, office@dcs-computing.com

    LIGGGHTS® is part of CFDEM®project:
    http://www.liggghts.com | http://www.cfdem.com

    Core developer and main author:
    Christoph Kloss, christoph.kloss@dcs-computing.com

    LIGGGHTS® is open-source, distributed under the terms of the GNU Public
    License, version 2 or later. It is distributed in the hope that it will
    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. You should have
    received a copy of the GNU General Public License along with LIGGGHTS®.
    If not, see http://www.gnu.org/licenses . See also top-level README
    and LICENSE files.

    LIGGGHTS® and CFDEM® are registered trade marks of DCS Computing GmbH,
    the producer of the LIGGGHTS® software and the CFDEM®coupling software
    See http://www.cfdem.com/terms-trademark-policy for details.

-------------------------------------------------------------------------
    Contributing author and copyright for this file:
    Christoph Kloss (DCS Computing GmbH, Linz)
    Christoph Kloss (JKU Linz)
    Richard Berger (JKU Linz)

    Copyright 2012-     DCS Computing GmbH, Linz
    Copyright 2009-2015 JKU Linz
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(insert/lozano,FixInsertLozano)

#else

#ifndef LMP_FIX_INSERT_LOZANO_H
#define LMP_FIX_INSERT_LOZANO_H

#include "fix_insert.h"
#include <list>

struct sLozanoParticle{
    double posX;
    double posY;
    double posZ;
    double radius;
};

namespace LAMMPS_NS {

class FixInsertLozano : public FixInsert {
 public:

  FixInsertLozano(class LAMMPS *, int, char **);
  ~FixInsertLozano();

  void init_defaults();
  void init();
  virtual void restart(char *);
 

  virtual BoundingBox getBoundingBox();

  void x_v_omega(int,int&,int&,double&);

  double insertion_fraction();
  int is_nearby(int);
  int calc_ninsert_this();
  virtual void calc_insertion_properties();

  std::list<sLozanoParticle> packAlgoLozano();

  // region to be used for insertion
  class Region *ins_region;
  char *idregion;
  double region_volume,region_volume_local;
  int ntry_mc;
  // for uniform distribution
  double radmin;
  double radmax;
  double type1_rad;
  double dens1, dens2;
  // for generalng
  double r1,r2,r3,r4,r5,r6;
  double p1,p2,p3,p4,p5,p6;
  double i_mean, i_sd, i_max, i_min;
  int s,SEED;

  // target that region should fulfil after each insertion
  double volumefraction_region;
  int ntotal_region;
  double masstotal_region;

  // ratio how many particles have been inserted
  double insertion_ratio;

  // enforce that
  bool check_dist_from_subdomain_border_;

  // warn if region extends outside box
  bool warn_region;
};

} // namespace LAMMPS_NS

#endif
#endif