#ifndef _GEN_NUM_H_
#define _GEN_NUM_H_

#include <string>

namespace PG
{

class NG
{
public:
 double m_min, m_max;
 std::string m_id;
public:
 NG(const NG &n, const int SEED);
 NG(double min, double max, const int SEED);
 virtual ~NG();
 void Reset();
 virtual double GetValue() = 0;
 virtual std::string ToString() = 0;
};

class UniformNG : public NG
{
public:
 UniformNG(const UniformNG &n, const int SEED);
 UniformNG(double min, double max, const int SEED);
 ~UniformNG();
 double GetValue();
 std::string ToString();
};

class BernoulliNG : public NG
{
public:
 double m_max_probability;
 BernoulliNG(const BernoulliNG &n, const int SEED);
 BernoulliNG(double min, double max, double max_probability, const int SEED);
 ~BernoulliNG();
 double GetValue();
 std::string ToString();
};

class GaussianNG : public NG
{
public:
 double m_mean, m_sd;
 GaussianNG(const GaussianNG &n, const int SEED);
 GaussianNG(double min, double max, double mean, double sd, const int SEED);
 ~GaussianNG();
 double GetValue();
 std::string ToString();
private:
 double BoxMuller(double mean, double stddev);
};

class GeneralNG : public NG
{
public:
 double* m_n;
 double* m_p;
 double* m_cp;
 size_t  m_size;
 GeneralNG(const double n[], const double p[], size_t size, const int SEED);
 GeneralNG(const GeneralNG &n, const int SEED);
 ~GeneralNG();
 double GetValue();
 std::string ToString();
};

class RangesNG : public NG
{
public:
 double* m_n;
 double* m_p;
 double* m_cp;
 size_t  m_size;
 RangesNG(const double n[], const double p[], size_t size, const int SEED);
 RangesNG(const RangesNG &n, const int SEED);
 ~RangesNG();
 double GetValue();
 std::string ToString();
};

class RangesGaussNG : public NG
{
public:
 double m_n;	// radius constant for particle one
 double m_mean, m_sd; // radius mean and standard deviation for particle 2
 double* m_p;	// probabilities for different particles
 double* m_cp;
 size_t m_size;
 RangesGaussNG(const double& n, const double p[], const double& mean,
  			   const double& sd, const double& min, const double& max, const int SEED);
 RangesGaussNG(const RangesGaussNG &n, const int SEED);
 ~RangesGaussNG();
 double GetValue();
 std::string ToString();

private:
 double Gaussian();

};

}

#endif /* _GEN_NUM_H_ */