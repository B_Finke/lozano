/* ----------------------------------------------------------------------
    This is the

    ██╗     ██╗ ██████╗  ██████╗  ██████╗ ██╗  ██╗████████╗███████╗
    ██║     ██║██╔════╝ ██╔════╝ ██╔════╝ ██║  ██║╚══██╔══╝██╔════╝
    ██║     ██║██║  ███╗██║  ███╗██║  ███╗███████║   ██║   ███████╗
    ██║     ██║██║   ██║██║   ██║██║   ██║██╔══██║   ██║   ╚════██║
    ███████╗██║╚██████╔╝╚██████╔╝╚██████╔╝██║  ██║   ██║   ███████║
    ╚══════╝╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝®

    DEM simulation engine, released by
    DCS Computing Gmbh, Linz, Austria
    http://www.dcs-computing.com, office@dcs-computing.com

    LIGGGHTS® is part of CFDEM®project:
    http://www.liggghts.com | http://www.cfdem.com

    Core developer and main author:
    Christoph Kloss, christoph.kloss@dcs-computing.com

    LIGGGHTS® is open-source, distributed under the terms of the GNU Public
    License, version 2 or later. It is distributed in the hope that it will
    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. You should have
    received a copy of the GNU General Public License along with LIGGGHTS®.
    If not, see http://www.gnu.org/licenses . See also top-level README
    and LICENSE files.

    LIGGGHTS® and CFDEM® are registered trade marks of DCS Computing GmbH,
    the producer of the LIGGGHTS® software and the CFDEM®coupling software
    See http://www.cfdem.com/terms-trademark-policy for details.

-------------------------------------------------------------------------
    Contributing author and copyright for this file:

    Christoph Kloss (DCS Computing GmbH, Linz)
    Christoph Kloss (JKU Linz)
    Richard Berger (JKU Linz)

    Copyright 2012-     DCS Computing GmbH, Linz
    Copyright 2009-2012 JKU Linz
------------------------------------------------------------------------- */
/*==========================================================================
############################################################################
#  _   ______      ____    ________                                        #
# |_| |   __  \   /    \  |__    __|             TU Braunschweig           #
#  _  |  |__|  | /  __  \    |  |                                          #
# | | |  _____/ /  /__\  \   |  |              LIGGGHTS extentions         #
# | | |  |     /  ______  \  |  |                                          #
# |_| |__|    /__/      \__\ |__|                     2017                 #
#                                                                          #
# Use under the terms of iPAT's general agreement on not beeing an asshole #
############################################################################

############################################################################
# Author: BeF 10-2017         Contact Model         Does it work? - yes    #
# Calculates attractive force before particle contact                      #
# See iPAT-Wiki for details                                                #
# Works with LIGGGHTS 3.7.0                                                #
############################################################################
==========================================================================*/

#ifdef NORMAL_MODEL
NORMAL_MODEL(VDW,vdw,6)
#else
#ifndef NORMAL_MODEL_VDW_H_
#define NORMAL_MODEL_VDW_H_
#include "global_properties.h"
#include "fix_property_atom.h"
#include <math.h>
#include <algorithm>
#include "normal_model_base.h"
#include "fix_mesh_surface.h"

namespace LIGGGHTS {

namespace ContactModels
{
  class ContactModelBase;

  template<>
  class NormalModel<VDW> : public NormalModelBase
  {
  public:
    NormalModel(LAMMPS * lmp, IContactHistorySetup* hsetup, class ContactModelBase *c) :
      NormalModelBase(lmp, hsetup, c),
      Hamaker(NULL), //Bene
      DistLimit(NULL), //Laura
      Yeff(NULL),
      Geff(NULL),
      betaeff(NULL),
      limitForce(false),
      displayedSettings(false),
      heating(false),
      heating_track(false),
      elastic_potential_offset_(-1),
      elasticpotflag_(false),
      fix_dissipated_(NULL),
      dissipatedflag_(false),
      overlap_offset_(0.0),
      disable_when_bonded_(false),
      bond_history_offset_(-1),
      dissipation_history_offset_(-1),
      cmb(c)
    {
      
    }

    void registerSettings(Settings & settings)
    {
      
      settings.registerOnOff("tangential_damping", tangential_damping, true);
      settings.registerOnOff("limitForce", limitForce);
      settings.registerOnOff("heating_normal_vdw",heating,false);
      settings.registerOnOff("heating_tracking",heating_track,false);
      settings.registerOnOff("computeElasticPotential", elasticpotflag_, false);
      settings.registerOnOff("computeDissipatedEnergy", dissipatedflag_, false);
      settings.registerOnOff("disableNormalWhenBonded", disable_when_bonded_, false);
      //TODO error->one(FLERR,"TODO here also check if right surface model used");
    }

    inline void postSettings(IContactHistorySetup * hsetup, ContactModelBase *cmb)
    {
        if (elasticpotflag_)
        {
            elastic_potential_offset_ = cmb->get_history_offset("elastic_potential_normal");
            if (elastic_potential_offset_ == -1)
            {
                elastic_potential_offset_ = hsetup->add_history_value("elastic_potential_normal", "0");
                hsetup->add_history_value("elastic_force_normal_0", "1");
                hsetup->add_history_value("elastic_force_normal_1", "1");
                hsetup->add_history_value("elastic_force_normal_2", "1");
                if (cmb->is_wall())
                    hsetup->add_history_value("elastic_potential_wall", "0");
                cmb->add_history_offset("elastic_potential_normal", elastic_potential_offset_);
            }
        }
        if (dissipatedflag_)
        {
            if (cmb->is_wall())
            {
                fix_dissipated_ = static_cast<FixPropertyAtom*>(modify->find_fix_property("dissipated_energy_wall", "property/atom", "vector", 0, 0, "dissipated energy"));
                dissipation_history_offset_ = cmb->get_history_offset("dissipation_force");
                if (!dissipation_history_offset_)
                    error->one(FLERR, "Internal error: Could not find dissipation history offset");
            }
            else
                fix_dissipated_ = static_cast<FixPropertyAtom*>(modify->find_fix_property("dissipated_energy", "property/atom", "vector", 0, 0, "dissipated energy"));
            if (!fix_dissipated_)
                error->one(FLERR, "Surface model has not registered dissipated_energy fix");
        }
        if (disable_when_bonded_)
        {
            bond_history_offset_ = cmb->get_history_offset("bond_contactflag");
            if (bond_history_offset_ < 0)
                error->one(FLERR, "Could not find bond history offset");
            overlap_offset_ = hsetup->add_history_value("overlap_offset", "0");
        }
    }

    void connectToProperties(PropertyRegistry & registry)
    {
      registry.registerProperty("Yeff", &MODEL_PARAMS::createYeff,"model vdw");
      registry.registerProperty("Geff", &MODEL_PARAMS::createGeff,"model vdw");
      registry.registerProperty("betaeff", &MODEL_PARAMS::createBetaEff,"model vdw");
      registry.registerProperty("Hamaker", &MODEL_PARAMS::createHamaker,"model vdw");//Bene
      registry.registerProperty("DistLimit", &MODEL_PARAMS::createDistLimit,"model vdw");//Laura

      registry.connect("Yeff", Yeff,"model vdw");
      registry.connect("Geff", Geff,"model vdw");
      registry.connect("betaeff", betaeff,"model vdw");
      registry.connect("Hamaker", Hamaker,"model vdw");//Bene
      registry.connect("DistLimit", DistLimit,"model vdw");//Laura

      // enlarge contact distance flag in case of elastic energy computation
      // to ensure that surfaceClose is called after a contact
      if (elasticpotflag_)
      {
          ///BE AWARE: THERE IS ANOTHER NEIGHBOUR LIST CUT OF HIDDEN SINCE LIG370. //Bene
          ///This one will only extend the cut of if elastic energy is computed
          //set neighbor contact_distance_factor here
          const char* neigharg[2];
          std::cout << " set elasticpotflag_ " << std::endl;
          neigharg[0] = "contact_distance_factor";
          neigharg[1] = "1.01";
          neighbor->modify_params(2,const_cast<char**>(neigharg));
    }

    //Bene Anfang
          //Strong enlargement of contact_distence_flag in order to always enter surface close function
          const char* neigharg[2];
          neigharg[0] = "contact_distance_factor";
          neigharg[1] = "1.6";
          neighbor->modify_params(2,const_cast<char**>(neigharg));

          //const char* nei[2];
          //nei[0] = "check";
          //nei[1] = "yes";
          //neighbor->modify_params(2,const_cast<char**>(nei));
    }
    //Bene Ende
    // effective exponent for stress-strain relationship
    
    inline double stressStrainExponent()
    {
      return 1.5;
    }

    void dissipateElasticPotential(SurfacesCloseData &scdata)
    {
        if (elasticpotflag_)
        {
            double * const elastic_energy = &scdata.contact_history[elastic_potential_offset_];
            if (scdata.is_wall)
            {
                // we need to calculate half an integration step which was left over to ensure no energy loss, but only for the elastic energy. The dissipation part is handled in fix_wall_gran_base.h.
                double delta[3];
                scdata.fix_mesh->triMesh()->get_global_vel(delta);
                vectorScalarMult3D(delta, update->dt);
                // -= because force is in opposite direction
                // no *dt as delta is v*dt of the contact position
                elastic_energy[0] -= (delta[0]*(elastic_energy[1]) +
                                      delta[1]*(elastic_energy[2]) +
                                      delta[2]*(elastic_energy[3]))*0.5
                                     // from previous half step
                                     + elastic_energy[4];
                elastic_energy[4] = 0.0;
            }
            elastic_energy[1] = 0.0;
            elastic_energy[2] = 0.0;
            elastic_energy[3] = 0.0;
        }
    }

    inline void surfacesIntersect(SurfacesIntersectData & sidata, ForceData & i_forces, ForceData & j_forces)
    {
     //if(sidata.rsq < sidata.radsum * sidata.radsum){
      //std::cout << " in surfacesIntersect " << std::endl;
      if (sidata.contact_flags) *sidata.contact_flags |= CONTACT_NORMAL_MODEL;
      const bool update_history = sidata.computeflag && sidata.shearupdate;

      const int itype = sidata.itype;
      const int jtype = sidata.jtype;
      const double radi = sidata.radi;
      const double radj = sidata.radj;
      double reff = sidata.is_wall ? radi : (radi*radj/(radi+radj));

      #ifdef SUPERQUADRIC_ACTIVE_FLAG
        if(sidata.is_non_spherical) {
          if(sidata.is_wall)
            reff = MathExtraLiggghtsNonspherical::get_effective_radius_wall(sidata, atom->roundness[sidata.i], error);
          else
            reff = MathExtraLiggghtsNonspherical::get_effective_radius(sidata, atom->roundness[sidata.i], atom->roundness[sidata.j], error);
        }
      #endif
      const double meff=sidata.meff;

      if(sidata.deltan < 0)
        error->one(FLERR, "sidata.deltan < 0!");
      const double sqrtval = sqrt(reff*sidata.deltan);
      #ifdef LIGGGHTS_DEBUG
        if(std::isnan(sqrtval))
          error->one(FLERR, "sqrtval is NaN!");
      #endif

      if (disable_when_bonded_ && update_history && sidata.deltan < sidata.contact_history[overlap_offset_])
        sidata.contact_history[overlap_offset_] = sidata.deltan;
      const double deltan = disable_when_bonded_ ? fmax(sidata.deltan-sidata.contact_history[overlap_offset_], 0.0) : sidata.deltan;

      const double Sn=2.*Yeff[itype][jtype]*sqrtval;
      const double St=8.*Geff[itype][jtype]*sqrtval;

      double kn=4./3.*Yeff[itype][jtype]*sqrtval;
      double kt=St;
      const double sqrtFiveOverSix = 0.91287092917527685576161630466800355658790782499663875;
      const double gamman=-2.*sqrtFiveOverSix*betaeff[itype][jtype]*sqrt(Sn*meff);
      const double gammat= tangential_damping ? -2.*sqrtFiveOverSix*betaeff[itype][jtype]*sqrt(St*meff) : 0.0;
      
      if(!displayedSettings)
      {
        displayedSettings = true;

        /*
        if(limitForce)
            if(0 == comm->me) fprintf(screen," NormalModel<HERTZ_STIFFNESS>: will limit normal force.\n");
        */
      }
      // convert Kn and Kt from pressure units to force/distance^2
      kn /= force->nktv2p;
      kt /= force->nktv2p;

      //Calculation of vdw-force during particle contact

      const double HAmaker = Hamaker[itype][jtype];//Bene
      const double dist_limit = std::min(DistLimit[itype], DistLimit[jtype]);//Laura
      //double F_vdw= HAmaker*radi*radj/(6*dist_limit*dist_limit*(radj+radj)); //Bene
      double F_vdw= -32*HAmaker*radi*radi*radi*radj*radj*radj*(radi+radj+dist_limit)/(3*dist_limit*dist_limit*(2*radi+2*radj+dist_limit)*(2*radi+2*radj+dist_limit)*(dist_limit*dist_limit+2*dist_limit*radi+2*dist_limit*radj+4*radi+radj)*(dist_limit*dist_limit+2*dist_limit*radi+2*dist_limit*radj+4*radi+radj)); //Bene

      const double Fn_damping = -gamman*sidata.vn;
      const double Fn_contact = kn*deltan+F_vdw; //Bene
      double Fn = Fn_damping + Fn_contact;

      //limit force to avoid the artefact of negative repulsion force
      /*if(limitForce && (Fn<0.0) )
      {
          Fn = 0.0;
      }
      */
      sidata.Fn = Fn;
      sidata.kn = kn;
      sidata.kt = kt;
      sidata.gamman = gamman;
      sidata.gammat = gammat;

      #ifdef NONSPHERICAL_ACTIVE_FLAG
          double torque_i[3] = {0., 0., 0.};
          double Fn_i[3] = { Fn * sidata.en[0], Fn * sidata.en[1], Fn * sidata.en[2]};
          if(sidata.is_non_spherical) {
            double xci[3];
            vectorSubtract3D(sidata.contact_point, atom->x[sidata.i], xci);
            vectorCross3D(xci, Fn_i, torque_i);
          }
          
      #endif

      // apply normal force
      if (!disable_when_bonded_ || sidata.contact_history[bond_history_offset_] < 0.5)
      {

        if(heating)
        {
            const double mj = sidata.is_wall ? sidata.mi : sidata.mj;
            const double E_therm = fabs((-sidata.vn - update->dt*Fn*0.5*(1.0/sidata.mi + 1.0/mj))*Fn_damping);
            sidata.P_diss += E_therm; 
            if(heating_track && sidata.is_wall)
                cmb->tally_pw(E_therm ,sidata.i,jtype,0);
            if(heating_track && !sidata.is_wall)
                cmb->tally_pp(E_therm ,sidata.i,sidata.j,0);
        }

        // energy balance terms
        if (update_history)
        {
            // compute increment in elastic potential
            if (elasticpotflag_)
            {
                double * const elastic_energy = &sidata.contact_history[elastic_potential_offset_];
                // correct for wall influence
                    double delta[3];
                if (sidata.is_wall)
                {
                    sidata.fix_mesh->triMesh()->get_global_vel(delta);
                    vectorScalarMult3D(delta, update->dt);
                    // -= because force is in opposite direction
                    // no *dt as delta is v*dt of the contact position
                    //printf("pela %e %e %e %e\n",  update->get_cur_time()-update->dt, deb, -sidata.radj, deb-sidata.radj);
                    elastic_energy[0] -= (delta[0]*elastic_energy[1] +
                                          delta[1]*elastic_energy[2] +
                                          delta[2]*elastic_energy[3])*0.5
                                         // from previous half step
                                         + elastic_energy[4];
                    elastic_energy[4] = -(delta[0]*Fn_contact*sidata.en[0] +
                                          delta[1]*Fn_contact*sidata.en[1] +
                                          delta[2]*Fn_contact*sidata.en[2])*0.5;
                }
                elastic_energy[1] = -Fn_contact*sidata.en[0];
                elastic_energy[2] = -Fn_contact*sidata.en[1];
                elastic_energy[3] = -Fn_contact*sidata.en[2];
            }
            // compute increment in dissipated energy
            if (dissipatedflag_)
            {
                double * const * const dissipated = fix_dissipated_->array_atom;
                double * const dissipated_i = dissipated[sidata.i];
                double * const dissipated_j = dissipated[sidata.j];
                const double F_diss = -Fn_damping;
                dissipated_i[1] += sidata.en[0]*F_diss;
                dissipated_i[2] += sidata.en[1]*F_diss;
                dissipated_i[3] += sidata.en[2]*F_diss;
                if (sidata.j < atom->nlocal && !sidata.is_wall)
                {
                    dissipated_j[1] -= sidata.en[0]*F_diss;
                    dissipated_j[2] -= sidata.en[1]*F_diss;
                    dissipated_j[3] -= sidata.en[2]*F_diss;
                }
                else if (sidata.is_wall)
                {
                    double * const diss_force = &sidata.contact_history[dissipation_history_offset_];
                    diss_force[0] -= sidata.en[0]*F_diss;
                    diss_force[1] -= sidata.en[1]*F_diss;
                    diss_force[2] -= sidata.en[2]*F_diss;
                }
            }
            #ifdef NONSPHERICAL_ACTIVE_FLAG
            if ((dissipatedflag_ || elasticpotflag_) && sidata.is_non_spherical)
                error->one(FLERR,"Dissipation and elastic potential do not compute torque influence for nonspherical particles");
            #endif
        }

        if(sidata.is_wall) {
          const double Fn_ = Fn * sidata.area_ratio;
          i_forces.delta_F[0] += Fn_ * sidata.en[0];
          i_forces.delta_F[1] += Fn_ * sidata.en[1];
          i_forces.delta_F[2] += Fn_ * sidata.en[2];
          #ifdef NONSPHERICAL_ACTIVE_FLAG
                  if(sidata.is_non_spherical) {
                    //for non-spherical particles normal force can produce torque!
                    i_forces.delta_torque[0] += torque_i[0];
                    i_forces.delta_torque[1] += torque_i[1];
                    i_forces.delta_torque[2] += torque_i[2];
                  }
          #endif
        } else {
        

        
          i_forces.delta_F[0] += sidata.Fn * sidata.en[0];//Bene
          i_forces.delta_F[1] += sidata.Fn * sidata.en[1];//Bene
          i_forces.delta_F[2] += sidata.Fn * sidata.en[2];//Bene

          j_forces.delta_F[0] += -i_forces.delta_F[0];//Bene
          j_forces.delta_F[1] += -i_forces.delta_F[1];//Bene
          j_forces.delta_F[2] += -i_forces.delta_F[2];//Bene
          #ifdef NONSPHERICAL_ACTIVE_FLAG
                  if(sidata.is_non_spherical) {
                    //for non-spherical particles normal force can produce torque!
                    double xcj[3], torque_j[3];
                    double Fn_j[3] = { -Fn_i[0], -Fn_i[1], -Fn_i[2]};
                    vectorSubtract3D(sidata.contact_point, atom->x[sidata.j], xcj);
                    vectorCross3D(xcj, Fn_j, torque_j);

                    i_forces.delta_torque[0] += torque_i[0];
                    i_forces.delta_torque[1] += torque_i[1];
                    i_forces.delta_torque[2] += torque_i[2];

                    j_forces.delta_torque[0] += torque_j[0];
                    j_forces.delta_torque[1] += torque_j[1];
                    j_forces.delta_torque[2] += torque_j[2];
                  }
          #endif
        }
      }
      else if (update_history)
      {
        sidata.contact_history[overlap_offset_] = sidata.deltan;
        dissipateElasticPotential(sidata);
      }
    }//}

    void surfacesClose(SurfacesCloseData &scdata, ForceData & i_forces, ForceData & j_forces)
    {
        if (scdata.contact_flags) *scdata.contact_flags |= CONTACT_NORMAL_MODEL;
        dissipateElasticPotential(scdata);

      using namespace std; //for screen output
      const int i = scdata.itype;
      const int j = scdata.jtype;
      //std::cout << " in surfacesClose " << std::endl;
      const double radi = scdata.radi;
      const double radj = scdata.radj;
      const double r = sqrt(scdata.rsq);
      const double dist =  r - (radi + radj);
      //const double wall_dist =  r - radi;
      const double rinv = 1.0 / r;
      const double dx = scdata.delta[0];
      const double dy = scdata.delta[1];
      const double dz = scdata.delta[2];
      const double enx = dx * rinv;
      const double eny = dy * rinv;
      const double enz = dz * rinv; 
      const double HAmaker = Hamaker[i][j];
      const double dist_limit = std::min(DistLimit[i], DistLimit[j]); //Laura
      double F_vdw = 0;
      //double dist_limit = 0.03; //possibly modification is needed (also in surface intersect function)
      
      //Limit of maximum vdw-force to ensure stability of simulation
      if(dist<dist_limit){

        F_vdw= -32*HAmaker*radi*radi*radi*radj*radj*radj*(radi+radj+dist_limit)/(3*dist_limit*dist_limit*(2*radi+2*radj+dist_limit)*(2*radi+2*radj+dist_limit)*(dist_limit*dist_limit+2*dist_limit*radi+2*dist_limit*radj+4*radi+radj)*(dist_limit*dist_limit+2*dist_limit*radi+2*dist_limit*radj+4*radi+radj)); //Bene
; 
        //cout << "WARNING: vdw-force was limited to maximum value. If this happens a lot consider changing distance limit for force maximum in normal_model_vdw.h line 467" << endl;

        }

      //regular calculation of vdw-force
      if(dist >= dist_limit){

        //F_vdw = -HAmaker*radi*radj/(6*dist*dist*(radj+radj));
        F_vdw= -32*HAmaker*radi*radi*radi*radj*radj*radj*(radi+radj+dist)/(3*dist*dist*(2*radi+2*radj+dist)*(2*radi+2*radj+dist)*(dist*dist+2*dist*radi+2*dist*radj+4*radi+radj)*(dist*dist+2*dist*radi+2*dist*radj+4*radi+radj)); //Bene
  

        }


        //calculate forces in each direction
        double fx = enx * F_vdw;
        double fy = eny * F_vdw;
        double fz = enz * F_vdw;
        
        
        
        //apply forces
        i_forces.delta_F[0] += fx;
        i_forces.delta_F[1] += fy;
        i_forces.delta_F[2] += fz;

        j_forces.delta_F[0] -= fx;
        j_forces.delta_F[1] -= fy;
        j_forces.delta_F[2] -= fz;

        //enable force update
        scdata.has_force_update = true;
    }

    void beginPass(SurfacesIntersectData&, ForceData&, ForceData&){}
    void endPass(SurfacesIntersectData&, ForceData&, ForceData&){}

  protected:
    double ** Yeff;
    double ** Geff;
    double ** betaeff;
    double ** Hamaker;
    double * DistLimit;

    bool tangential_damping;
    bool limitForce;
    bool displayedSettings;
    bool heating;
    bool heating_track;
    int elastic_potential_offset_;
    bool elasticpotflag_;
    FixPropertyAtom *fix_dissipated_;
    bool dissipatedflag_;
    int overlap_offset_;
    bool disable_when_bonded_;
    int bond_history_offset_;
    int dissipation_history_offset_;
    class ContactModelBase *cmb;

  };

}

}
#endif
#endif
