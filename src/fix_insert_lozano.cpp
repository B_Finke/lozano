/* ----------------------------------------------------------------------
    This is the

    ██╗     ██╗ ██████╗  ██████╗  ██████╗ ██╗  ██╗████████╗███████╗
    ██║     ██║██╔════╝ ██╔════╝ ██╔════╝ ██║  ██║╚══██╔══╝██╔════╝
    ██║     ██║██║  ███╗██║  ███╗██║  ███╗███████║   ██║   ███████╗
    ██║     ██║██║   ██║██║   ██║██║   ██║██╔══██║   ██║   ╚════██║
    ███████╗██║╚██████╔╝╚██████╔╝╚██████╔╝██║  ██║   ██║   ███████║
    ╚══════╝╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝®

    DEM simulation engine, released by
    DCS Computing Gmbh, Linz, Austria
    http://www.dcs-computing.com, office@dcs-computing.com

    LIGGGHTS® is part of CFDEM®project:
    http://www.liggghts.com | http://www.cfdem.com

    Core developer and main author:
    Christoph Kloss, christoph.kloss@dcs-computing.com

    LIGGGHTS® is open-source, distributed under the terms of the GNU Public
    License, version 2 or later. It is distributed in the hope that it will
    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. You should have
    received a copy of the GNU General Public License along with LIGGGHTS®.
    If not, see http://www.gnu.org/licenses . See also top-level README
    and LICENSE files.

    LIGGGHTS® and CFDEM® are registered trade marks of DCS Computing GmbH,
    the producer of the LIGGGHTS® software and the CFDEM®coupling software
    See http://www.cfdem.com/terms-trademark-policy for details.

-------------------------------------------------------------------------
    Contributing author and copyright for this file:
    Christoph Kloss (DCS Computing GmbH, Linz)
    Christoph Kloss (JKU Linz)
    Richard Berger (JKU Linz)

    Copyright 2012-     DCS Computing GmbH, Linz
    Copyright 2009-2015 JKU Linz
------------------------------------------------------------------------- */

#include <math.h>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include "fix_insert_lozano.h"
#include "atom.h"
#include "atom_vec.h"
#include "force.h"
#include "update.h"
#include "comm.h"
#include "modify.h"
#include "region.h"
#include "domain.h"
#include "random_park.h"
#include "memory.h"
#include "error.h"
#include "fix_particledistribution_discrete.h"
#include "fix_template_sphere.h"
#include "vector_liggghts.h"
#include "mpi_liggghts.h"
#include "particleToInsert.h"
#include "fix_multisphere.h"
#include "math_extra_liggghts.h"
#include "gen_pack.h"

#define SEED_OFFSET 12

using namespace LAMMPS_NS;
using namespace FixConst;

/* ---------------------------------------------------------------------- */

FixInsertLozano::FixInsertLozano(LAMMPS *lmp, int narg, char **arg) :
    FixInsert(lmp, narg, arg)
    ,ins_region(NULL)
{
      // set defaults first, then parse args
  init_defaults();
    // parse args

  bool hasargs = true;
  while(iarg < narg && hasargs)
  {
    hasargs = false;
    if (strcmp(arg[iarg],"region") == 0) {
      if (iarg+2 > narg) error->fix_error(FLERR,this,"");
      int iregion = domain->find_region(arg[iarg+1]);
      if (iregion == -1)
        error->fix_error(FLERR,this,"region ID does not exist");
      int n = strlen(arg[iarg+1]) + 1;
      idregion = new char[n];
      strcpy(idregion,arg[iarg+1]);
      ins_region = domain->regions[iregion];
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"particles_in_region") == 0) {
      if (iarg+2 > narg)
        error->fix_error(FLERR,this,"");
      ntotal_region = atoi(arg[iarg+1]);
      if(ntotal_region <= 0)
        error->fix_error(FLERR,this,"'ntotal_region' > 0 required");
      iarg += 2;
      hasargs = true;
    }else if (strcmp(arg[iarg],"Seed") == 0) {
      if (iarg+2 > narg)
        error->fix_error(FLERR,this,"");
      SEED = atoi(arg[iarg+1]);
      if(SEED <= 0)
        error->fix_error(FLERR,this,"Seed required");
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"ntry_mc") == 0) {
      if (iarg+2 > narg) error->fix_error(FLERR,this,"");
      ntry_mc = atoi(arg[iarg+1]);
      if(ntry_mc < 1000)
        error->fix_error(FLERR,this,"ntry_mc must be > 1000");
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"radiusmin") == 0) {
      if (iarg+2 > narg)
        error->fix_error(FLERR,this,"");
      radmin = atof(arg[iarg+1]);
      if(radmin < 0.)
        error->fix_error(FLERR,this,"'radiusmin' > 0 required");
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"radiusmax") == 0) {
      if (iarg+2 > narg)
        error->fix_error(FLERR,this,"");
      radmax = atof(arg[iarg+1]);           // arguments needed for lozano algorithm atof imports float atoi imports int
      if(radmax < 0.)
        error->fix_error(FLERR,this,"'radiusmax' > 0 required");
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"type1radius") == 0) {
      if (iarg+2 > narg)
        error->fix_error(FLERR,this,"");
      type1_rad = atof(arg[iarg+1]);
      if(type1_rad < 0.)
        error->fix_error(FLERR,this,"'type1radius' > 0 required");
      iarg += 2;
      hasargs = true;
    } else if (strcmp(arg[iarg],"density") == 0) {
      if (iarg+3 > narg) error->fix_error(FLERR,this,"not enough keyword for 'density'");
      if (iarg+3 > narg){
        error->fix_error(FLERR,this,"");
      dens1 = atof(arg[iarg+1]);
      dens2 = atof(arg[iarg+2]);
      iarg += 3;
      hasargs = true;
    }}  else if (strcmp(arg[iarg],"ranges5") == 0) {
      if (iarg+8 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"five") == 0)  {
          s = atoi(arg[iarg+2]);                  // first argument is size then the different rad sizes
          r1 = atof(arg[iarg+3]);
          r2 = atof(arg[iarg+4]);
          r3 = atof(arg[iarg+5]);
          r4 = atof(arg[iarg+6]);  
          r5 = atof(arg[iarg+7]);
          iarg += 8;
          hasargs = true; 
      }} else if (strcmp(arg[iarg],"probabilities5") == 0) {
      if (iarg+7 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"five") == 0)  {
          p1 = atof(arg[iarg+2]);
          p2 = atof(arg[iarg+3]);
          p3 = atof(arg[iarg+4]);
          p4 = atof(arg[iarg+5]);  
          p5 = atof(arg[iarg+6]);
          iarg += 7;
          hasargs = true; 
      }}  else if (strcmp(arg[iarg],"ranges2") == 0) {
      if (iarg+5 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"two") == 0)  {
          s = atoi(arg[iarg+2]);                  // first argument is size then the different rad sizes
          r1 = atof(arg[iarg+3]);
          r2 = atof(arg[iarg+4]);
          iarg += 5;
          hasargs = true; 
      }} else if (strcmp(arg[iarg],"probabilities2") == 0) {
      if (iarg+4 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"two") == 0)  {
          p1 = atof(arg[iarg+2]);
          p2 = atof(arg[iarg+3]);
          iarg += 4;
          hasargs = true; 
      }}  else if (strcmp(arg[iarg],"ranges6") == 0) {
      if (iarg+9 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"six") == 0)  {
          s = atoi(arg[iarg+2]);                  // first argument is size then the different rad sizes
          r1 = atof(arg[iarg+3]);
          r2 = atof(arg[iarg+4]);
          r3 = atof(arg[iarg+5]);
          r4 = atof(arg[iarg+6]);  
          r5 = atof(arg[iarg+7]);
          r6 = atof(arg[iarg+8]);
          iarg += 9;
          hasargs = true; 
      }} else if (strcmp(arg[iarg],"probabilities6") == 0) {
      if (iarg+8 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"six") == 0)  {
          p1 = atof(arg[iarg+2]);
          p2 = atof(arg[iarg+3]);
          p3 = atof(arg[iarg+4]);
          p4 = atof(arg[iarg+5]);  
          p5 = atof(arg[iarg+6]);
          p6 = atof(arg[iarg+7]);
          iarg += 8;
          hasargs = true; 
      }} else if (strcmp(arg[iarg],"RangesGauss") == 0) {
      if (iarg+7> narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"5"))  {
          std::cout << "in Ranges get value" << std::endl;
          r1 = atof(arg[iarg+2]);
          i_mean = atof(arg[iarg+3]);
          i_sd = atof(arg[iarg+4]);
          i_min = atof(arg[iarg+5]);
          i_max = atof(arg[iarg+6]);
          iarg += 7;
          hasargs = true; 
      }} else if (strcmp(arg[iarg],"ProbGauss") == 0) {
      if (iarg+4 > narg) error->fix_error(FLERR,this,"not enough keyword for 'ranges'");
      if (strcmp(arg[iarg+1],"2"))  {
          p1 = atof(arg[iarg+2]);
          p2 = atof(arg[iarg+3]);
          iarg += 4;
          hasargs = true; 
      }} else if(strcmp(style,"insert/lozano") == 0)
        error->fix_error(FLERR,this,"unknown keyword");
    }

  // no fixed total number of particles inserted by this fix exists
  if(strcmp(style,"insert/lozano") == 0)
    ninsert_exists = 0;
}

void FixInsertLozano::init_defaults()
{
      ins_region = NULL;
      idregion = 0;
      ntry_mc = 100000;

      type1_rad = 0.0;
      radmin = 0.0;
      radmax = 0.0;

      volumefraction_region = 0.0;
      ntotal_region = 0;
      masstotal_region = 0.0;



      region_volume = region_volume_local = 0.;

      insertion_ratio = 0.;

      check_dist_from_subdomain_border_ = true;

      warn_region = true;


}

FixInsertLozano::~FixInsertLozano()
{
    if(idregion) delete []idregion;
}

void FixInsertLozano::init()
{
    FixInsert::init();

    if (ins_region)
    {
        int iregion = domain->find_region(idregion);
        if (iregion == -1)
            error->fix_error(FLERR,this,"regions used by this command must not be deleted");
        ins_region = domain->regions[iregion];
    }

    if(!ins_region)
        error->fix_error(FLERR,this,"must define an insertion region");
}

BoundingBox FixInsertLozano::getBoundingBox() 
{
  BoundingBox bb(ins_region->extent_xlo, ins_region->extent_xhi,
                 ins_region->extent_ylo, ins_region->extent_yhi,
                 ins_region->extent_zlo, ins_region->extent_zhi);

  //double extend = 2*maxrad /*cut*/ + this->extend_cut_ghost(); 
  //bb.shrinkToSubbox(domain->sublo, domain->subhi);
  //bb.extendByDelta(extend);
  //std::cout << "in getBoundingBox" << std::endl;
  return bb;
}
 // not used
double FixInsertLozano::insertion_fraction()
{
   return 1;
} // not used
inline int FixInsertLozano::is_nearby(int i)
{

    return 1;
} // not used
void FixInsertLozano::calc_insertion_properties()
{
    int laura = 1;
}
//number of particles to insert this timestep
int FixInsertLozano::calc_ninsert_this()
{
    //problem wenn wert > oder < dem wirklichen insert n ist?
    
    return ntotal_region;
}    

void FixInsertLozano::x_v_omega(int ninsert_this_local,int &ninserted_this_local, int &ninserted_spheres_this_local, double &mass_inserted_this_local)
{
    ninserted_this_local = ninserted_spheres_this_local = 0;
    mass_inserted_this_local = 0.;
    
    int type;
    double density;
    double radius;
    double pos[3];
    double omega_insert[3];
    double quat_insert[4];

    double v_insert[3];
    vectorZeroize3D(v_insert);
    std::list<sLozanoParticle> part = packAlgoLozano();     // result of packing stored in part
    
    int nLozano = part.size();
    std::cout << "size of lozano list stored " << nLozano << std::endl;
    std::cout << "particle number assumed to be stored " << ntotal_region << std::endl;
    
    ParticleToInsert *pti;
    int i = 0;

    for (std::list<sLozanoParticle>::const_iterator it = part.begin(); it != part.end(); it++) // iterator used to write values from part list in arrays
    {   

        radius = it->radius;

        if(radius == type1_rad)             // if radius is like set as type1 rad declare as type 2 else type 1 
        {
            type = 2;
            density = dens2;
        }
        else
        {
            type = 1;
            density = dens1;
        }
        pti = fix_distribution->pti_list[i];                        // pti points at position in list where the particle properties should be stored
        
        pos[0]= it->posX;
        pos[1]= it->posY;
        pos[2]= it->posZ;
        v_insert[0] = 0;                                            // velocity omega and quat is set zero
        v_insert[1] = 0;
        v_insert[2] = 0;
        omega_insert[0] = 0;
        omega_insert[1] = 0;
        omega_insert[2] = 0;
        quat_insert[0] = 0;
        quat_insert[1] = 0;
        quat_insert[2] = 0;
        quat_insert[3] = 0;
       
        ninserted_this_local += pti->set_t_r_x_v_omega(type,density,radius,pos,v_insert,omega_insert,quat_insert); // own set method to also set type and radius defined in particletoinsert.cpp
        
        i++;    // increment counter       
    }
}

std::list<sLozanoParticle> FixInsertLozano::packAlgoLozano()
{
    std::list<sLozanoParticle> res;
    BoundingBox bb = getBoundingBox();                      // get boundingbox method is called

    double lo[3], hi[3];
    bb.getBoxBounds(lo, hi);                                // box bounds are stored in lo and hi
    //double ramin = radmin;
    //double ramax = radmax;

    //std::cout << "in packAlgoLozano" << std::endl;
    /*std::cout << "low " << lo[0] << ";" << lo[1] <<";" << lo[2] << std::endl;
    std::cout << "high " << hi[0] << ";" << hi[1] <<";" << hi[2] << std::endl;
    std::cout << "radmin: " << radmin << "radmax: " << radmax << std::endl;
    std::cout << "p " << p1 << ";" << p2 <<";" << p3 <<";" << p4 <<";" << p5 << std::endl;*/

    PG::Container* container = new PG::Box({lo[0], lo[1], lo[2]},           // box is initilized with lo and hi values as bounds for lozano insertion
                                        {hi[0], hi[1], hi[2]});
    //PG::Container* container = new PG::Box({1., 1., 1.},
                                        //{1.8, 1.8, 1.8});
    PG::NG* ng = NULL;
    std::cout << i_mean << std::endl;
    if(radmin != 0)
    {
        double rmin(radmin), rmax(radmax);
        ng = new PG::UniformNG(rmin, rmax, SEED);                             // different NG's can be used uniform or ranges etc.
    }
    else if(s == 5)
    {
        double n[5]={r1,r2,r3,r4,r5};
        double p[5]={p1,p2,p3,p4,p5};
        size_t size(s);
        ng = new PG::GeneralNG(n,p,size,SEED);
    }
    else if(s == 6)
    {
        double n[6]={r1,r2,r3,r4,r5,r6};
        double p[6]={p1,p2,p3,p4,p5,p6};
        size_t size(s);
        ng = new PG::GeneralNG(n,p,size,SEED);
    }
    else if(s == 2)
    {
        double n[2]={r1,r2};
        double p[2]={p1,p2};
        size_t size(s);
        ng = new PG::GeneralNG(n,p,size,SEED);
    }
    else if(i_mean != 0)
    {
        double n=r1;
        double p[2]={p1,p2};
        double mean = i_mean;
        double sd = i_sd;
        double min = i_min;
        double max = i_max;
        ng = new PG::RangesGaussNG(n,p,mean,sd,min,max,SEED);
    }


    PG::Grid3d dom;
    PG::SpherePack* pack = new PG::SpherePack();
    PG::SpherePackStat result = PG::GenerateSpherePack(container, ng, &dom, pack, SEED);

    std::cout << "Size of lozano list ist: " << static_cast<int>(pack->s.size()) << std::endl;      // number of particles that were generated

    for(std::vector<PG::Sphere>::const_iterator it = pack->s.begin(); it != pack->s.end(); it++) // store particle properties in res list
    {
        sLozanoParticle tempParticle;   // list with struct properties of slozanoparticle defined in header
        tempParticle.posX = it->x;
        //std::cout << tempParticle.posX << std::endl;
        tempParticle.posY = it->y;
        tempParticle.posZ = it->z;
        tempParticle.radius = it->r;
        res.push_back(tempParticle);            // adds new values in res list
    }
    
    std::cout << "end of algo" << std::endl;

/*
 const char* filename = "box.txt";
 remove(filename);
 std::ofstream myfile;
 myfile.open(filename);
 myfile << std::setprecision(15);                                   // how many decimal characters (floating point precision)

 myfile << "LIGGGHTS data file from restart file: timestep = 1000, procs = 12" << "\n" << "\n";
 myfile << static_cast<int>(pack->s.size()) << " " << "atoms" << "\n" << "\n";
 myfile << "2 atoms types" << "\n" << "\n";
 myfile << "20 extra bond per atom" << "\n" << "\n";


 myfile << -1.0 << " " << 1.0 << " " << "xlo" << " " << "xhi" << "\n"; // change box size
 myfile << -1.0 << " " << 1.0 << " " << "ylo" << " " << "yhi" << "\n";
 myfile << -1.0 << " " << 1.0 << " " << "zlo" << " " << "zhi" << "\n" << "\n";
 myfile << "Atoms" << "\n" << "#"<< "id" << " " << "type" << " " << "x" << " " << "y" << " " << "z" << " " << "radius" << " " << "density" << " " << "0" << "\n";

 int i = 1;
 for (std::vector<PG::Sphere>::const_iterator it = pack->s.begin(); it != pack->s.end(); it++)
 {
  //std::cout << "Writing sphere to file." << std::endl;s
  myfile << i << " " << 1 << " " << it->x << " " << it->y << " " << it->z << " " << it->r <<  " " << 2500 << " " << 0 << "\n";                  // change density
  i += 1;
 }

 std::cout << "Writing to file finished" << std::endl;

 myfile.close(); */

    delete container;
    delete ng;
    delete pack;
    
    return res;
}

void FixInsertLozano::restart(char *buf)
{
    FixInsert::restart(buf);

    ins_region->reset_random(seed + SEED_OFFSET);
}

